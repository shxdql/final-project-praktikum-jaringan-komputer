using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace server
{
    class Program
    {
        private static void ProcessClientRequests(object argument)
        {
            TcpClient client = (TcpClient)argument;
            try
            {

                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());

                // welcome word
                string name = reader.ReadLine();
                Console.WriteLine("Connected with " + name);
                Console.WriteLine(" ");
                writer.WriteLine("Welcome to the game," + name + "!");
                writer.Flush();
                writer.WriteLine("Type many of alphabets do you  want");
                writer.Flush();
                writer.WriteLine("to get bigger score than your opponent.");
                writer.Flush();
                writer.WriteLine("DO NOT REPEAT THE SAME ALPHABET.");
                writer.Flush();
                writer.WriteLine("And after that, there will be");
                writer.Flush();
                writer.WriteLine("3 question about simple math.");
                writer.Flush();
                writer.WriteLine("If you can answer it with the");
                writer.Flush();
                writer.WriteLine("correct one, you will get your score.");
                writer.Flush();
                writer.WriteLine("If you can't answer the question, you");
                writer.Flush();
                writer.WriteLine("will get directed to the first question");
                writer.Flush();

                //alphabet game
                int nilai = 0;
                int skor = 0;
                string totalword;
                int total;
                totalword = reader.ReadLine();
                int.TryParse(totalword, out total);
                Console.WriteLine(name + " wants " + total + " alphabets.");
                for (int i = 0; i < total; i++)
                {
                    string alpha = reader.ReadLine();
                    Console.WriteLine((i + 1) + ". " + name + " = " + alpha);
                    if (alpha == "A" || alpha == "K" || alpha == "U")
                    {
                        nilai = 1;
                    }
                    else if (alpha == "B" || alpha == "L" || alpha == "V")
                    {
                        nilai = 2;
                    }
                    else if (alpha == "C" || alpha == "M" || alpha == "W")
                    {
                        nilai = 3;
                    }
                    else if (alpha == "D" || alpha == "N" || alpha == "X")
                    {
                        nilai = 4;
                    }
                    else if (alpha == "E" || alpha == "O" || alpha == "Y")
                    {
                        nilai = 5;
                    }
                    else if (alpha == "F" || alpha == "P" || alpha == "Z")
                    {
                        nilai = 6;
                    }
                    else if (alpha == "G" || alpha == "Q")
                    {
                        nilai = 7;
                    }
                    else if (alpha == "H" || alpha == "R")
                    {
                        nilai = 8;
                    }
                    else if (alpha == "I" || alpha == "S")
                    {
                        nilai = 9;
                    }
                    else if (alpha == "J" || alpha == "T")
                    {
                        nilai = 10;
                    }

                    skor += nilai;
                    Console.WriteLine(name + "'s Score = " + skor);

                }

                // simple math game
                writer.WriteLine("Value of each alphabet is the same as the order");
                writer.Flush();
                writer.WriteLine("For Example:");
                writer.Flush();
                writer.WriteLine("A = 1, B = 2, C = 3");
                writer.Flush();
                writer.WriteLine(" ");
                writer.Flush();
                for (int i = 0; i < 3; i++)
                {
                    String opr = String.Empty;
                    String alphabet1 = String.Empty;
                    String alphabet2 = String.Empty;
                    int alphascore = 0;
                    int alphascore1 = 0;
                    int alphascore2 = 0;
                    Random rndm = new Random();
                    var rand_alpha1 = rndm.Next(1, 27);
                    var rand_alpha2 = rndm.Next(1, 27);
                    var rand_opr = rndm.Next(1, 5);
                    switch (rand_alpha1)
                    {
                        case 1:
                            alphabet1 = "A";
                            alphascore1 = 1;
                            break;
                        case 2:
                            alphabet1 = "B";
                            alphascore1 = 2;
                            break;
                        case 3:
                            alphabet1 = "C";
                            alphascore1 = 3;
                            break;
                        case 4:
                            alphabet1 = "D";
                            alphascore1 = 4;
                            break;
                        case 5:
                            alphabet1 = "E";
                            alphascore1 = 5;
                            break;
                        case 6:
                            alphabet1 = "F";
                            alphascore1 = 6;
                            break;
                        case 7:
                            alphabet1 = "G";
                            alphascore1 = 7;
                            break;
                        case 8:
                            alphabet1 = "H";
                            alphascore1 = 8;
                            break;
                        case 9:
                            alphabet1 = "I";
                            alphascore1 = 9;
                            break;
                        case 10:
                            alphabet1 = "J";
                            alphascore1 = 10;
                            break;
                        case 11:
                            alphabet1 = "K";
                            alphascore1 = 11;
                            break;
                        case 12:
                            alphabet1 = "L";
                            alphascore1 = 12;
                            break;
                        case 13:
                            alphabet1 = "M";
                            alphascore1 = 13;
                            break;
                        case 14:
                            alphabet1 = "N";
                            alphascore1 = 14;
                            break;
                        case 15:
                            alphabet1 = "O";
                            alphascore1 = 15;
                            break;
                        case 16:
                            alphabet1 = "P";
                            alphascore1 = 16;
                            break;
                        case 17:
                            alphabet1 = "Q";
                            alphascore1 = 17;
                            break;
                        case 18:
                            alphabet1 = "R";
                            alphascore1 = 18;
                            break;
                        case 19:
                            alphabet1 = "S";
                            alphascore1 = 19;
                            break;
                        case 20:
                            alphabet1 = "T";
                            alphascore1 = 20;
                            break;
                        case 21:
                            alphabet1 = "U";
                            alphascore1 = 21;
                            break;
                        case 22:
                            alphabet1 = "V";
                            alphascore1 = 22;
                            break;
                        case 23:
                            alphabet1 = "W";
                            alphascore1 = 23;
                            break;
                        case 24:
                            alphabet1 = "X";
                            alphascore1 = 24;
                            break;
                        case 25:
                            alphabet1 = "Y";
                            alphascore1 = 25;
                            break;
                        case 26:
                            alphabet1 = "Z";
                            alphascore1 = 26;
                            break;
                        default:
                            break;
                    }
                    switch (rand_alpha2)
                    {
                        case 1:
                            alphabet2 = "A";
                            alphascore2 = 1;
                            break;
                        case 2:
                            alphabet2 = "B";
                            alphascore2 = 2;
                            break;
                        case 3:
                            alphabet2 = "C";
                            alphascore2 = 3;
                            break;
                        case 4:
                            alphabet2 = "D";
                            alphascore2 = 4;
                            break;
                        case 5:
                            alphabet2 = "E";
                            alphascore2 = 5;
                            break;
                        case 6:
                            alphabet2 = "F";
                            alphascore2 = 6;
                            break;
                        case 7:
                            alphabet2 = "G";
                            alphascore2 = 7;
                            break;
                        case 8:
                            alphabet2 = "H";
                            alphascore2 = 8;
                            break;
                        case 9:
                            alphabet2 = "I";
                            alphascore2 = 9;
                            break;
                        case 10:
                            alphabet2 = "J";
                            alphascore2 = 10;
                            break;
                        case 11:
                            alphabet2 = "K";
                            alphascore2 = 11;
                            break;
                        case 12:
                            alphabet2 = "L";
                            alphascore2 = 12;
                            break;
                        case 13:
                            alphabet2 = "M";
                            alphascore2 = 13;
                            break;
                        case 14:
                            alphabet2 = "N";
                            alphascore2 = 14;
                            break;
                        case 15:
                            alphabet2 = "O";
                            alphascore2 = 15;
                            break;
                        case 16:
                            alphabet2 = "P";
                            alphascore2 = 16;
                            break;
                        case 17:
                            alphabet2 = "Q";
                            alphascore2 = 17;
                            break;
                        case 18:
                            alphabet2 = "R";
                            alphascore2 = 18;
                            break;
                        case 19:
                            alphabet2 = "S";
                            alphascore2 = 19;
                            break;
                        case 20:
                            alphabet2 = "T";
                            alphascore2 = 20;
                            break;
                        case 21:
                            alphabet2 = "U";
                            alphascore2 = 21;
                            break;
                        case 22:
                            alphabet2 = "V";
                            alphascore2 = 22;
                            break;
                        case 23:
                            alphabet2 = "W";
                            alphascore2 = 23;
                            break;
                        case 24:
                            alphabet2 = "X";
                            alphascore2 = 24;
                            break;
                        case 25:
                            alphabet2 = "Y";
                            alphascore2 = 25;
                            break;
                        case 26:
                            alphabet2 = "Z";
                            alphascore2 = 26;
                            break;
                        default:
                            break;
                    }

                    switch (rand_opr)
                    {
                        case 1:
                            opr = "+";
                            alphascore = alphascore1 + alphascore2;
                            break;
                        case 2:
                            opr = "-";
                            alphascore = alphascore1 - alphascore2;
                            break;
                        case 3:
                            opr = "*";
                            alphascore = alphascore1 * alphascore2;
                            break;
                        case 4:
                            opr = "/";
                            alphascore = alphascore1 / alphascore2;
                            break;
                        default:
                            break;
                    }
                    Console.WriteLine(" ");
                    string jawabanstring;
                    int jawaban;
                    Console.WriteLine("Question for " + name);
                    Console.WriteLine(alphabet1 + " " + opr + " " + alphabet2 + " = " + alphascore);
                    writer.WriteLine(alphabet1 + " " + opr + " " + alphabet2 + " = ");
                    writer.Flush();
                    jawabanstring = reader.ReadLine();
                    int.TryParse(jawabanstring, out jawaban);
                    Console.WriteLine(name + " = " + jawaban);
                    if (jawaban == alphascore)
                    {
                        Console.WriteLine(name + "'s Answer is CORRECT.");
                        writer.WriteLine("Your Answer is CORRECT.");
                        writer.Flush();
                    }
                    else
                    {
                        Console.WriteLine(name + "'s Answer is INCORRECT.");
                        writer.WriteLine("Your Answer is INCORRECT.");
                        writer.Flush();
                        i = -1;
                    }
                }
                Console.WriteLine(" ");
                writer.WriteLine("Your Score is " + skor);
                writer.Flush();
                writer.WriteLine("Thank you for playing this game");
                writer.Flush();
                writer.Write("Press Any Key . . .");
                writer.Flush();
                reader.Close();
                writer.Close();
                client.Close();
                Console.WriteLine(name + " Disconnected . . .");
            }
            catch (IOException)
            {
                Console.WriteLine("Disconnected . . .");
                Console.WriteLine(" ");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }


        public static void Main()
        {
            TcpListener listener = null;
            try
            {
                int i = 0;
                string playerString;
                int player = 0;
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 6969);
                listener.Start();
                Console.WriteLine("Server Started!");
                Console.Write("How many player do you want to join the game?");
                playerString = Console.ReadLine();
                int.TryParse(playerString, out player);
                Console.WriteLine("Waiting for player . . .");
                while (i < player)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    Thread t = new Thread(ProcessClientRequests);
                    t.Start(client);
                    player++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }
    }
}
