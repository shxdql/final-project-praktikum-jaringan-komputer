using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

public class EchoClient
{
    public static void Main()
    {
        try
        {
            string ipAdd;
            string portstring;
            int port;
            Console.WriteLine("Which IP Address and port do you want to connect?");
            Console.Write("IP Address = ");
            ipAdd = Console.ReadLine();
            Console.Write("Port = ");
            portstring = Console.ReadLine();
            int.TryParse(portstring, out port);
            TcpClient client = new TcpClient(ipAdd, port);
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());

            String name = String.Empty;
            Console.WriteLine("=====================================");
            Console.Write("Enter Your Name = ");
            name = Console.ReadLine();
            writer.WriteLine(name);
            writer.Flush();
            Console.WriteLine(" ");
            for (int i = 0; i < 10; i++)
            {
                string welcome = reader.ReadLine();
                Console.WriteLine(welcome);
            }
            Console.WriteLine("=====================================");
            Console.WriteLine(" ");
            Console.WriteLine("=====================================");

            String alpha = String.Empty;
            string closing;
            string totalword;
            int total;
            Console.Write("Enter the total of the alphabets = ");
            totalword = Console.ReadLine();
            writer.WriteLine(totalword);
            writer.Flush();
            int.TryParse(totalword, out total);

            for (int i = 0; i < total; i++)
            {
                Console.Write("Enter your alphabet (Capitalize) = ");
                alpha = Console.ReadLine();
                writer.WriteLine(alpha);
                writer.Flush();

            }
            Console.WriteLine(" ");

            for (int i = 0; i < 4; i++)
            {
                string guide;
                guide = reader.ReadLine();
                Console.WriteLine(guide);
            }

            for (int i = 0; i < 3; i++)
            {
                string soal;
                string jawaban;
                string status;
                soal = reader.ReadLine();
                Console.Write(soal);
                jawaban = Console.ReadLine();
                writer.WriteLine(jawaban);
                writer.Flush();
                status = reader.ReadLine();
                Console.WriteLine(status);
                if (status == "Your Answer is INCORRECT.")
                {
                    i = -1;
                }
            }
            Console.WriteLine(" ");

            for (int i = 0; i < 2; i++)
            {
                closing = reader.ReadLine();
                Console.WriteLine(closing);
            }
            Console.WriteLine("=====================================");
            closing = reader.ReadLine();
            Console.Write(closing);
            Console.ReadKey();
            reader.Close();
            writer.Close();
            client.Close();

        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}
